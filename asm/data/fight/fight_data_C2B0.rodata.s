.include "macro.inc"

.section .rodata

dlabel D_80392650
.double 1.99

dlabel D_80392658
.double 3.3

dlabel D_80392660
.byte 0x3F, 0xD9, 0x99, 0x99, 0x99, 0x99, 0x99, 0x9C, 0x3F, 0x33, 0x33, 0x33

dlabel jtbl_8039266C
.word L803905D4_A1E4, L80390648_A258, L8039073C_A34C, L803907D4_A3E4, L80390804_A414, L803908BC_A4CC, L80390B78_A788, L80390C48_A858, L80390D20_A930, L80390E1C_AA2C, L80390EF8_AB08, L80390F7C_AB8C

dlabel D_8039269C
.float 0.66, 0.1

dlabel D_803926A4
.float 0.4

dlabel D_803926A8
.word 0x4CBEBC20, 0x00000000, 0x3FD99999, 0x9999999A

dlabel D_803926B8
.float 1023.0, 0.0

dlabel D_803926C0
.double 0.04

dlabel D_803926C8
.float 0.1

dlabel D_803926CC
.float 5000.0

dlabel D_803926D0
.float 12000.0, 0.0

dlabel D_803926D8
.double 0.11

dlabel D_803926E0
.double 4.2

dlabel D_803926E8
.float 2500.0

dlabel D_803926EC
.float 0.1

dlabel D_803926F0
.float 5000.0

dlabel D_803926F4
.float 12000.0

dlabel D_803926F8
.float 0.66, 0.0

dlabel D_80392700
.double 0.11

dlabel D_80392708
.double 4.2

dlabel D_80392710
.float 2500.0, 0.0

dlabel D_80392718
.double 0.11

dlabel D_80392720
.float 3.3

dlabel D_80392724
.float 4.62, 0.0, 0.0
